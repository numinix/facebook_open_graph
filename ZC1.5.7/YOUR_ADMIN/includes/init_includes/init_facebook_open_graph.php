<?php

if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
}

$module_constant = 'FACEBOOK_OPEN_GRAPH_VERSION';
$module_installer_directory = DIR_FS_ADMIN . 'includes/installers/facebook_open_graph';
$module_name = "Facebook Open Graph";
$zencart_com_plugin_id = 0; // from zencart.com plugins - Leave Zero not to check
//Just change the stuff above... Nothing down here should need to change

$configuration_group_id = '';
if (defined($module_constant)) {
    $current_version = constant($module_constant);return;
} else {
    
    global $sniffer;
    
    $current_version = "0.0.0";

    if (!$sniffer->field_exists(TABLE_PRODUCTS, 'stored_url')) $db->Execute("ALTER TABLE ".TABLE_PRODUCTS." ADD stored_url varchar(200) NULL default NULL;");

    $query = $db->Execute("SELECT configuration_group_id FROM configuration_group WHERE configuration_group_title= '$module_name' LIMIT 1");
    while(!$query->EOF) {
        $configuration_group_id = $query->fields['configuration_group_id'];
    }

    if($configuration_group_id > 0) {
        $delete = $db->Execute("
            DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
            DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
        ");
    } else {
        $db->Execute("
            INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) 
            VALUES (NULL, 'Facebook Open Graph', 'Set Facebook Open Graph Options', '1', '1');"
        );

        $configuration_group_id = $db->Insert_ID();

    }

    $db->Execute("UPDATE configuration_group SET sort_order = ".$configuration_group_id." WHERE configuration_group_id = ".$configuration_group_id.";");
    if(!defined('FACEBOOK_OPEN_GRAPH_VERSION')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Version', 'FACEBOOK_OPEN_GRAPH_VERSION', '1.2.2', 'Version Installed:', ".$configuration_group_id.", 0, NOW(), NULL, NULL);");
    if(!defined('FACEBOOK_OPEN_GRAPH_STATUS')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Enable Facebook Open Graph', 'FACEBOOK_OPEN_GRAPH_STATUS', 'true', 'Enable Facebook Open Graph meta data?', ".$configuration_group_id.", 10, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');");
    if(!defined('FACEBOOK_OPEN_GRAPH_APPID')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Application ID', 'FACEBOOK_OPEN_GRAPH_APPID', '', 'Please enter your application ID (<a href=\"http://developers.facebook.com/setup/\" target=\"_blank\">Get an application ID</a>)', ".$configuration_group_id.", 30, NOW(), NULL, NULL);");
    if(!defined('FACEBOOK_OPEN_GRAPH_APPSECRET')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Application Secret', 'FACEBOOK_OPEN_GRAPH_APPSECRET', '', 'Please enter your application secret', ".$configuration_group_id.", 30, NOW(), NULL, NULL);");
    if(!defined('FACEBOOK_OPEN_GRAPH_ADMINID')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Admin ID', 'FACEBOOK_OPEN_GRAPH_ADMINID', '', 'Enter the Admin ID(s) of the Facebook user(s) that administer your Facebook fan page separated by commas (<a href=\"http://www.facebook.com/insights/\" target=\"_blank\">Insights for your domain</a>)', ".$configuration_group_id.", 40, NOW(), NULL, NULL);");
    if(!defined('FACEBOOK_OPEN_GRAPH_DEFAULT_IMAGE')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Default Image', 'FACEBOOK_OPEN_GRAPH_DEFAULT_IMAGE', '', 'Enter the full path to your default image or leave blank to disable.  The default image is only used when the product image cannot be found.', ".$configuration_group_id.", 50, NOW(), NULL, NULL);");
    if(!defined('FACEBOOK_OPEN_GRAPH_TYPE')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Type', 'FACEBOOK_OPEN_GRAPH_TYPE', 'website', 'Enter an Open Graph type for your products (<a href=\"http://developers.facebook.com/docs/opengraph#types\" target=\"_blank\">Open Graph Types</a>)', ".$configuration_group_id.", 120, NOW(), NULL, NULL);");
    if(!defined('FACEBOOK_OPEN_GRAPH_CPATH')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Use cPath', 'FACEBOOK_OPEN_GRAPH_CPATH', 'true', 'Include the cPath in your URLs?', ".$configuration_group_id.", 130, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');");
    if(!defined('FACEBOOK_OPEN_GRAPH_LANGUAGE')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Include Language', 'FACEBOOK_OPEN_GRAPH_LANGUAGE', 'false', 'Include the language in your URLs?', ".$configuration_group_id.", 140, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');");
    if(!defined('FACEBOOK_OPEN_GRAPH_CANONICAL')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Use Canonical URL', 'FACEBOOK_OPEN_GRAPH_CANONICAL', 'true', 'Use the canonical URL from ZC 1.3.9 or try and recreate the URL?', ".$configuration_group_id.", 150, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');");
    if(!defined('FACEBOOK_OPEN_GRAPH_STORED_URL')) $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES (NULL, 'Use Stored URL', 'FACEBOOK_OPEN_GRAPH_STORED_URL', 'false', 'Use the stored URL from Numinix Product Fields?', ".$configuration_group_id.", 160, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');");

        # Register the configuration page for Admin Access Control
    $db->Execute("INSERT IGNORE INTO admin_pages (page_key,language_key,main_page,page_params,menu_key,display_on_menu,sort_order) VALUES ('configFacebookOpenGraph','BOX_CONFIGURATION_FACEBOOKOPENGRAPH','FILENAME_CONFIGURATION',CONCAT('gID=',".$configuration_group_id."),'configuration','Y',".$configuration_group_id.");");

}

$installers = scandir($module_installer_directory, 1);

$newest_version = $installers[0];
$newest_version = substr($newest_version, 0, -4);

sort($installers);
if (version_compare($newest_version, $current_version) > 0) {
    foreach ($installers as $installer) {
        if (version_compare($newest_version, substr($installer, 0, -4)) >= 0 && version_compare($current_version, substr($installer, 0, -4)) < 0) {
            include($module_installer_directory . '/' . $installer);
            $current_version = str_replace("_", ".", substr($installer, 0, -4));
            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '" . $current_version . "' WHERE configuration_key = '" . $module_constant . "' LIMIT 1;");
            $messageStack->add("Installed " . $module_name . " v" . $current_version, 'success');
        }
    }
}

if (!function_exists('plugin_version_check_for_updates')) {
    function plugin_version_check_for_updates($fileid = 0, $version_string_to_check = '') {
        if ($fileid == 0){
            return FALSE;
        }
        $new_version_available = FALSE;
        $lookup_index = 0;
        $url = 'http://www.zen-cart.com/downloads.php?do=versioncheck' . '&id=' . (int) $fileid;
        $data = json_decode(file_get_contents($url), true);
        if (!$data || !is_array($data)) return false;
        // compare versions
        if (version_compare($data[$lookup_index]['latest_plugin_version'], $version_string_to_check) > 0) {
            $new_version_available = TRUE;
        }
        // check whether present ZC version is compatible with the latest available plugin version
        if (!in_array('v' . PROJECT_VERSION_MAJOR . '.' . PROJECT_VERSION_MINOR, $data[$lookup_index]['zcversions'])) {
            $new_version_available = FALSE;
        }
        if ($version_string_to_check == true) {
            return $data[$lookup_index];
        } else {
            return FALSE;
        }
    }
}

// Version Checking
if ($zencart_com_plugin_id != 0) {
    if ($_GET['gID'] == $configuration_group_id) {
        $new_version_details = plugin_version_check_for_updates($zencart_com_plugin_id, $current_version);
        if ($new_version_details != FALSE) {
            $messageStack->add("Version " . $new_version_details['latest_plugin_version'] . " of " . $new_version_details['title'] . ' is available at <a href="' . $new_version_details['link'] . '" target="_blank">[Details]</a>', 'caution');
        }
    }
}