SET @configuration_group_id=0;
SELECT (@configuration_group_id:=configuration_group_id) 
FROM configuration_group 
WHERE configuration_group_title= 'Facebook Open Graph' 
LIMIT 1;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Use Stored URL', 'FACEBOOK_OPEN_GRAPH_STORED_URL', 'false', 'Use the stored URL from Numinix Product Fields?', @configuration_group_id, 160, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Application Secret', 'FACEBOOK_OPEN_GRAPH_APPSECRET', '', 'Please enter your application secret', @configuration_group_id, 30, NOW(), NULL, NULL);

ALTER TABLE products ADD stored_url varchar(200) NULL default NULL;