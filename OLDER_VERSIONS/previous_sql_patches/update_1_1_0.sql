SET @configuration_group_id=0;
SELECT (@configuration_group_id:=configuration_group_id) 
FROM configuration_group 
WHERE configuration_group_title= 'Facebook Open Graph' 
LIMIT 1;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Version', 'FACEBOOK_OPEN_GRAPH_VERSION', '1.1.0', 'Version Installed:', @configuration_group_id, 0, NOW(), NULL, NULL);

ALTER TABLE products ADD stored_url varchar(200) NULL default NULL;

# Register the configuration page for Admin Access Control
INSERT IGNORE INTO admin_pages (page_key,language_key,main_page,page_params,menu_key,display_on_menu,sort_order) VALUES ('configFacebookOpenGraph','BOX_CONFIGURATION_FACEBOOKOPENGRAPH','FILENAME_CONFIGURATION',CONCAT('gID=',@configuration_group_id),'configuration','Y',@configuration_group_id);
