# FACEBOOK OPEN GRAPH
#

ALTER TABLE products ADD stored_url varchar(200) NULL default NULL;

SELECT (@configuration_group_id:=configuration_group_id) 
FROM configuration_group 
WHERE configuration_group_title= 'Facebook Open Graph' 
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'Facebook Open Graph', 'Set Facebook Open Graph Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Version', 'FACEBOOK_OPEN_GRAPH_VERSION', '1.2.2', 'Version Installed:', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Enable Facebook Open Graph', 'FACEBOOK_OPEN_GRAPH_STATUS', 'true', 'Enable Facebook Open Graph meta data?', @configuration_group_id, 10, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Application ID', 'FACEBOOK_OPEN_GRAPH_APPID', '', 'Please enter your application ID (<a href="http://developers.facebook.com/setup/" target="_blank">Get an application ID</a>)', @configuration_group_id, 30, NOW(), NULL, NULL),
(NULL, 'Application Secret', 'FACEBOOK_OPEN_GRAPH_APPSECRET', '', 'Please enter your application secret', @configuration_group_id, 30, NOW(), NULL, NULL),
(NULL, 'Admin ID', 'FACEBOOK_OPEN_GRAPH_ADMINID', '', 'Enter the Admin ID(s) of the Facebook user(s) that administer your Facebook fan page separated by commas (<a href="http://www.facebook.com/insights/" target="_blank">Insights for your domain</a>)', @configuration_group_id, 40, NOW(), NULL, NULL),
(NULL, 'Default Image', 'FACEBOOK_OPEN_GRAPH_DEFAULT_IMAGE', '', 'Enter the full path to your default image or leave blank to disable.  The default image is only used when the product image cannot be found.', @configuration_group_id, 50, NOW(), NULL, NULL),
(NULL, 'Type', 'FACEBOOK_OPEN_GRAPH_TYPE', 'website', 'Enter an Open Graph type for your products (<a href="http://developers.facebook.com/docs/opengraph#types" target="_blank">Open Graph Types</a>)', @configuration_group_id, 120, NOW(), NULL, NULL),
(NULL, 'Use cPath', 'FACEBOOK_OPEN_GRAPH_CPATH', 'true', 'Include the cPath in your URLs?', @configuration_group_id, 130, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Include Language', 'FACEBOOK_OPEN_GRAPH_LANGUAGE', 'false', 'Include the language in your URLs?', @configuration_group_id, 140, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Use Canonical URL', 'FACEBOOK_OPEN_GRAPH_CANONICAL', 'true', 'Use the canonical URL from ZC 1.3.9 or try and recreate the URL?', @configuration_group_id, 150, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Use Stored URL', 'FACEBOOK_OPEN_GRAPH_STORED_URL', 'false', 'Use the stored URL from Numinix Product Fields?', @configuration_group_id, 160, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');

# Register the configuration page for Admin Access Control
INSERT IGNORE INTO admin_pages (page_key,language_key,main_page,page_params,menu_key,display_on_menu,sort_order) VALUES ('configFacebookOpenGraph','BOX_CONFIGURATION_FACEBOOKOPENGRAPH','FILENAME_CONFIGURATION',CONCAT('gID=',@configuration_group_id),'configuration','Y',@configuration_group_id);
