<?php
//Added files compatible with ZenCart 1.5.8

if(version_compare(PROJECT_VERSION_MAJOR.".".PROJECT_VERSION_MINOR, "1.5.0") >= 0) { 
    // continue Zen Cart 1.5.0
      // add to configuration menus
    if (function_exists('zen_page_key_exists') && function_exists('zen_register_admin_page') && !zen_page_key_exists('configFacebookOpenGraph')) {
      zen_register_admin_page('configFacebookOpenGraph',
                              'BOX_CONFIGURATION_FACEBOOKOPENGRAPH', 
                              'FILENAME_CONFIGURATION',
                              'gID='.(int)$configuration_group_id, 
                              'configuration', 
                              'Y',
                              999);
        
      $messageStack->add('FOG Configuration menu.', 'success');
    }
  }